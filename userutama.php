<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laman Utama | Pengguna</title>


<style type="text/css"></style>
<script type="text/javascript" src="SpryAssets/calender/datetimepicker_css.js"></script>
<script type="text/javascript" src="SpryAssets/jquery.js"></script>
<script src="SpryAssets/includes/ice/ice.js" type="text/javascript"></script>
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#topnav li").prepend("<span></span>"); //Throws an empty span tag right before the a tag
	$("#topnav li").each(function() { //For each list item...
		var linkText = $(this).find("a").html(); //Find the text inside of the <a> tag
		$(this).find("span").show().html(linkText); //Add the text in the <span> tag
	});
	$("#topnav li").hover(function() {	//On hover...
		$(this).find("span").stop().animate({
			marginTop: "-40" //Find the <span> tag and move it up 40 pixels
		}, 240);
	} , function() { //On hover out...
		$(this).find("span").stop().animate({
			marginTop: "0"  //Move the <span> back to its original state (0px)
		}, 240);
	});
});
</script>
<link href="SpryAssets/rfnet.css" rel="stylesheet" type="text/css">
<link href="SpryAssets/CMS Style Sheet.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function display(obj,id1,id2,id3,id4,id5,id6,id7) {
txt = obj.options[obj.selectedIndex].value;
document.getElementById(id1).style.display = 'none';
document.getElementById(id2).style.display = 'none';
document.getElementById(id3).style.display = 'none';
document.getElementById(id4).style.display = 'none';
document.getElementById(id5).style.display = 'none';
document.getElementById(id6).style.display = 'none';
document.getElementById(id7).style.display = 'none';
if ( txt.match(id1) ) {
document.getElementById(id1).style.display = 'block';
}
if ( txt.match(id2) ) {
document.getElementById(id2).style.display = 'block';
}
if ( txt.match(id3) ) {
document.getElementById(id3).style.display = 'block';
}
if ( txt.match(id4) ) {
document.getElementById(id4).style.display = 'block';
}
if ( txt.match(id5) ) {
document.getElementById(id5).style.display = 'block';
}
if ( txt.match(id6) ) {
document.getElementById(id6).style.display = 'block';
}
if ( txt.match(id7) ) {
document.getElementById(id7).style.display = 'block';
}
}

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
function MM_jumpMenuGo(objId,targ,restore){ //v9.0
  var selObj = null;  with (document) { 
  if (getElementById) selObj = getElementById(objId);
  if (selObj) eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0; }
}
</script>

<style type="text/css">
body {
	background-color: #CCCCCC;
}
</style>
<style type="text/css">
.style1 {
	font-size: 18px
}
.style2 {
	color: #0066FF;
	font-weight: bold;
}
.style10 {font-size: 12; font-weight: bold; }
.style13 {font-size: 12px}
</style>
<link href="SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css">
<link href="SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style31 {font-size: 9px}
.style33 {font-size: 9px; color: #FF0000; }
-->
</style>
</head>
<body>
<div id="Wrapper">
  <div id="Header">
    <div id="Banner"></div>
    <div id="NavContainer">
      <!--setup navi bar-->
              <ul id="topnav">
                <li> <a href="usertentangkami.php"> Tentang Kami </a></li>
                <li><a href="userbantuan.php"> Bantuan</a></li>
				<li> <a href="login.php"> Pentadbir </a></li>
      </ul>
    </div>
            <!--end navi bar-->

  </div><!--endheader-->


  <div id="Contentbox">

    <div id="FlyData">
    <marquee scrolldelay="100" class="Title" style="FONT-SIZE: x-medium">
                Selamat Datang ke Sistem Pengesahan Kehadiran J-BioTech | Welcome To Validation Attendent System J-BioTech
      </marquee>
  </div>
    <!--endFlyData-->
    <div class="LeftPanel" id="LeftPanel">    
      <div>
        <div align="center">
          <ul id="MenuBar1" class="MenuBarVertical">
            <li><a href="userutama.php">Laman Utama</a></li>
            <li><a href="userpendaftaran.php">Pendaftaran Seminar</a></li>
            <li><a href="usermaklumat.php">Maklumat Peserta</a></li>
            <li><a href="userkehadiran.php">Sahkan Kehadiran</a></li>
            <div align="center"></div>
          </ul>
        </div>
      </div>
    </div>
    <!--endLeftPanel-->
  <div class="RightContent" id="RightContent">
    <p><span class="Title style1">LAMAN UTAMA</span></p>
    <fieldset>
    <legend class="style2">Maklumat Sistem </legend>
    <form name="form1" method="post" action="">
      <div><br>
        <table width="731" border="0">
          <tr>
            <td width="12">&nbsp;</td>
            <td width="690" class="style13"><div align="justify" class="style13">Selamat datang ke sistem pengesahan kehadiran seminar/program/majlis J-BioTech. Sistem ini di wujudkan adalah bertujuan untuk memudahkan peserta seminar/program/majlis yang dianjurkan oleh pihak J-Biotech untuk melakukan pendaftaran untuk menghadiri seminar/program/majlis dan mengesahkan kehadiran peserta tersebut. Melalui sistem ini, peserta dapat melihat program yang dianjurkan oleh pihak J-Biotech dan membuat penyertaan melalui online. Segala urusan mengenai seminar yang berkaitan dengan pendaftaran seminar/program/majlis dan pengesahan kehadiran boleh dilakukan secara online melalui sistem ini.</div><br></td>
            <td width="14">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="style13"><div align="justify" class="style13">Untuk memudahkan pengguna sistem ini, pengguna boleh menekan butang <a href="userbantuan.php">bantuan</a><a href="userbantuan.php"><span class="style33">[tekan disini] </span></a>untuk mendapatkan bantuan untuk menggunakan sistem ini. Pengguna akan dapat mengetahui cara-cara yang betul untuk menggunakan sistem ini. Pengguna boleh mengetahui lebih lanjut mengenai J-Biotech dengan menekan butang <a href="usertentangkami.php">Tentang Kami<span class="style33">[tekan disini]</span></a>. Melalui Butang ini, pengguna dapat mengetahui semua butiran mengenai J-Biotech untuk memudahkan urusan pertanyaan sebarang masalah dan sebagainya.</div></td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <p>&nbsp;</p>
    </form>
    </fieldset>
    </div>
  <!--endRightContent-->
  </div>
  <!--endContentbox-->
  <div id="footer">
    <form id="form3" name="form3" method="post" action="">
      <div align="left"></div>
      <div align="right"></div>
      <div align="right"> </div>
      <p align="left">&nbsp;</p>
    </form>
  </div>
</div>
<!--endWrapper-->
<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</body>
</html>